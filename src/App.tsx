import Router from './router/Index';
import Footer from './view/layout/Footer';
import Header from './view/layout/Header';

function App() {
  return (
    <>
      <Header/>
      <Router/>
      <Footer/>
    </>
  );
}

export default App;
