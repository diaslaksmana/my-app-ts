import React from 'react'

export const LoadingSkeleton = () => {
  return (
    <div className="loading-skeleton">
      <div className="loading-skeleton__row">
        <div className="loading-skeleton__line"></div>
        <div className="loading-skeleton__line"></div>
      </div>
      <div className="loading-skeleton__row">
        <div className="loading-skeleton__line"></div>
        <div className="loading-skeleton__line"></div>
      </div>
    </div>
  )
}
export default LoadingSkeleton