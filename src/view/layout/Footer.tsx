import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { MovieModel } from '../../utils/interface/movie';
import { fetchData } from '../../utils/service/movie.service';

export const Footer = () => {
  const [genreMovie, setGenreMovie] = useState<MovieModel[]>([]);
  const [genreTv, setGenreTv] = useState<MovieModel[]>([]);
  useEffect(() => {
    async function getData() {
      const result = await fetchData('genre/movie/list');
      setGenreMovie(result.genres);
      console.log(result)
    }
    async function getData2() {
      const result = await fetchData('genre/tv/list');
      setGenreTv(result.genres);
      console.log(result)
    }
    
    getData();
    getData2();
  }, []);

  if (genreMovie.length === 0) {
    return <div>Loading...</div>;
  }
  return (
   <footer id='footer' className='footer'>
      <div className='footer__top bg-dark-700 '>
        <div className='container mx-auto py-5 px-3 sm-px-0'> 
          <div className="grid grid-cols-2 gap-4 border-b mb-5">
            <div className='text-left'>
              <h1 className=' brand text-3xl text-white'>Movie</h1>              
            </div>
            <div className='text-right text-white'>
              <ul className='flex gap-4 justify-end'>
                <li>
                  <Link to='' className='text-xs'>Instagram</Link>
                </li>
                <li>
                  <Link to='' className='text-xs'>Twitter</Link>
                </li>
                <li>
                  <Link to='' className='text-xs'>Facebook</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="block sm:flex gap-4">
            <div className='text-left mb-3  basis-full sm:basis-[40%]'>
              <h1 className='text-xl text-white'>Movie Categories</h1>
              <ul className='columns-2'>
                {genreMovie?.map((genre) => (   
                  <li className=' ' key={genre.id}>
                    <Link to={`/moviecategory/${genre.id}`} className='text-white text-sm'>{genre.name}</Link>
                  </li>
                 ))}
              </ul>
            </div>
            <div className='mb-3 basis-full sm:basis-[40%]'>
              <h1 className='text-xl text-white'>Tv Series</h1> 
              <ul className='columns-2'>
                {genreTv?.map((genre) => (   
                  <li className=' ' key={genre.id}>
                    <Link to={`/tvcategory/${genre.id}`} className='text-white text-sm'>{genre.name}</Link>
                  </li>
                 ))}
              </ul>
            </div>
            <div className='mb-3 sm:pl-5 basis-full sm:flex-auto sm:border-l'>
              <h1 className='text-xl text-white'>Support</h1> 
              <ul>
                <li>
                  <Link to='' className='text-white text-sm'>My Account</Link>
                </li>
                <li>
                  <Link to='' className='text-white text-sm'>FAQ</Link>
                </li>
                <li>
                  <Link to='' className='text-white text-sm'>Help Center</Link>
                </li>
                <li>
                  <Link to='' className='text-white text-sm'>About</Link>
                </li>
                <li>
                  <Link to='' className='text-white text-sm'>Contact</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className='footer__bottom bg-dark-800'>
        <div className='container mx-auto py-5 px-3 sm-px-0'> 
            <p className='text-white text-xs text-center'>Copyright © 2023, Movie. All Rights Reserved</p>
        </div>        
      </div>

   </footer>
  )
}

export default Footer