
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/swiper.css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

// import required modules
import { Autoplay, Pagination, Navigation } from 'swiper';
import { useRef } from 'react';
import { Link } from 'react-router-dom';
import { BACKDROP_SIZE, IMAGE_BASE_URL } from '../../utils/config/Config';
import useFetchGeneral from '../../hooks/useFetchGeneral';
import LoadingSection from '../component/loading/LoadingSection';

export const Hero = () => {

  const {upcoming,isLoading,error} = useFetchGeneral();
  const progressCircle = useRef<any>(null);
  const progressContent = useRef<any>(null);
  const onAutoplayTimeLeft = (s:any, time:any, progress:any) => {
    progressCircle.current.style.setProperty('--progress', 1 - progress);
    progressContent.current.textContent = `${Math.ceil(time / 1000)}s`;
  };

  if (isLoading) {
    return <LoadingSection/>
  }

  if (error) {
    return <div>Error: {error}</div>;
  }


  return (
    <section id='hero' className='hero' >
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        onAutoplayTimeLeft={onAutoplayTimeLeft}
        className="mySwiper"
      >
        {upcoming?.map((movie) => (
          <SwiperSlide key={movie.id}>
            <div className='card-hero flex items-center justify-center'>
              <div className='thumbnail  z-0 w-full h-screen'>             
                <img alt={movie.title} src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}`+ movie.backdrop_path}  className=" object-cover w-full h-screen"/>
              </div>
              <div className='overview absolute z-10 w-[80%] pt-40'>
                <h1 className='text-white font-bold text-4xl'>{movie?.media_type === "tv" ? movie?.name :movie?.title}</h1>
                <p className='text-white sm:text-base text-sm max-w-[80%] m-auto '>{movie.overview}</p>
                <p className='text-white mb-5 max-w-[80%] m-auto'><b>Release:</b> { movie?.media_type === "tv" ? movie?.first_air_date :movie?.release_date}</p>
              <Link  to={movie?.media_type === "tv" ? (`/tv/${movie.id}`) :(`/movie/${movie.id}`) } className='btn btn__primary rounded'>Watching</Link>
              </div>
            </div>
          </SwiperSlide>
        ))}
        <div className="autoplay-progress" slot="container-end">
          <svg viewBox="0 0 48 48" ref={progressCircle}>
            <circle cx="24" cy="24" r="20"></circle>
          </svg>
          <span ref={progressContent}></span>
        </div>
      </Swiper>
    </section>
   
  )
}

export default Hero