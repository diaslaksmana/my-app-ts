import { Link } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import SearchIcon from '../../assets/svg/SearchIcon'
import useFetchHeader from '../../hooks/useFetchHeader'
import '../../style/layout/header.scss'
import { IMAGE_BASE_URL, POSTER_SIZE } from '../../utils/config/Config'

export const Header = () => {
    const{data,value,onChange}=useFetchHeader();


 
  return (
    <header id='header'className={`header fixed w-full top-0 z-[99999] `}>
        <nav>
                <div className="block sm:flex flex-row gap-4 py-5 items-center content-center container mx-auto  " >
                    <div className='header__left px-3 sm:px-0 flex gap-3 basis-full items-center justify-center  sm:basis-2/5'>
                        <div className='brand'>
                            <NavLink to='/'>
                              <h1 className='text-4xl font-bold text-white '>Movie</h1>
                            </NavLink>                           
                        </div>
                        <div className='search  w-full  relative'>
                            <div className='flex items-center'>
                            <input className='border w-full p-1 bg-[transparent] rounded-full pl-3 border-white' placeholder='Find your favorite movie' value={value ? value : ''} onChange={onChange}/>
                            <SearchIcon />
                        </div>

                        <div className='suggestion'>
                            
                            {value === '' ? null : 
                                <>
                                    {/* @ts-ignore*/}
                                    {data?.filter((item) => {
                                        if (value === '') {
                                            return data;
                                        }else if (item?.title?.toLowerCase().includes(value?.toLowerCase()) || item?.name?.toLowerCase().includes(value?.toLowerCase())) {
                                            return item;
                                            {/* @ts-ignore*/}
                                        }}).map((item, id) => (
                                            <div className="suggestion-link " key={id}>                                             
                                            <Link  to={item?.first_air_date ? (`/tv/${item.id}`) :(`/movie/${item.id}`)} className='flex'  >
                                            <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ item.poster_path} className='w-[50px] h-[70px] mr-5 object-cover object-center rounded-md'/> 
                                            {item.title || item.name} -  {item.media_type}
                                            </Link>                                 
                                        </div>
                                    ))}
                                </>
                            }
                        </div>

                        </div>                        
                    </div>
                   <div className='header__right bg-dark-800 sm:bg-[transparent] basis-full sm:basis-3/5'>
                        <div className='menu'>
                            <ul className='flex list-none gap-5 items-center justify-center sm:justify-end'>
                                <li>
                                    <NavLink to="/about" className='text-white'>About</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/moviecategory" className='text-white'>Movie</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/tvcategory" className='text-white'>Series</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/kids" className='text-white'>Kids</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/wishlist" className='text-white'>Wishlist</NavLink>
                                </li>                                
                                <li>
                                    <NavLink to="/login" className='text-white'>Login</NavLink>
                                </li>
                            </ul>
                        </div>
                   </div>
                </div>
        </nav>
    </header>
  )
}

export default Header