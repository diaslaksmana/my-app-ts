import { NavLink } from 'react-router-dom';
import { Link } from 'react-router-dom'
import { useFetchCategory } from '../../../hooks/useFetchCategory';
import {IMAGE_BASE_URL, POSTER_SIZE } from '../../../utils/config/Config';
import { convertRated } from '../../../utils/helper/Helper';
import Loading from '../../component/loading/Loading';
import LoadingSection from '../../component/loading/LoadingSection';

export const MovieCategory = () => {
  const {error,isLoading,movies,genres,handleGenre,pages,num, setNum} =useFetchCategory();  

    if (isLoading) {
      return <Loading/>
    }  
    if (error) {
      return <div>Error: {error}</div>;
    }
  return (
    <div id='category'>
        <div className='container mx-auto px-3 lg:px-0 category'>
            <div className='aside sticky'>
                <h1 className='text-2xl'>Category</h1>
                <div className='catogory__Genres'>
                    <ul>
                       {/* @ts-ignore*/}
                        {genres?.map((genre) => (   
                        <li className='mb-2' key={genre.id}>
                            <NavLink  to={`/moviecategory/${genre.id}`} onClick={handleGenre} className='text-white text-sm hover:text-[#575A4E]'> 
                                {genre.name}
                            </NavLink>
                        </li>
                        ))} 
                    </ul>
                </div>
                
            </div>
            <div className='content'>
                <h1 className='text-2xl'>
                    Movie List
                </h1>
                
                <div className='card-movie'>          
                {isLoading ?  (<LoadingSection/>)   :
                (
                  <div className="grid grid-cols-2 md:grid-cols-3  lg:grid-cols-5 gap-4  ">
                    {/* @ts-ignore*/}
                    { movies?.map((movie,index) => (
                      <div className='card' key={index}>
                        <figure className="snip1527">
                          <div className="image"><img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} className='w-[100%] h-[350px] object-cover object-center rounded-md'/></div>
                          <figcaption>
                          <div className="date"><span className="day">{convertRated(movie.vote_average)}</span></div>
                            <h3>{movie.title}</h3>
                            <p>
                              {movie.overview.slice(0, 80) + `...`}
                            </p>
                          </figcaption>
                          <Link  to={`/movie/${movie.id}`}></Link>
                        </figure>
                      </div>   
                    ))}              
                  </div>
                )       
               
                }
              {/* Pagination */}

          
              <div className='pagination my-14'>
                <ul className='flex flex-row justify-center items-center pl-0 gap-5 '>
                  <li>
                    <button 
                      className='next'
                      disabled={  pages?.page == 1}
                      onClick={() => setNum(num - 1)}
                    >
                      Prev
                    </button>
                  </li>
                  <li>
                    <div className='current-page'> 
                      <span className='font-bold'>{pages?.page}</span>
                    </div>
                  </li>
                  <li>
                    <button 
                      className='prev'
                      disabled={pages?.page === pages?.total_pages}
                      onClick={() => setNum(num + 1)}
                    >
                      Next
                    </button>
                  </li>
                </ul>
              </div>
            </div>

            </div>
        </div>
    </div>
  )
}
