import { useEffect, useState } from 'react'
import List from '../../assets/svg/List';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../../utils/config/Config';
import { MovieModel } from '../../utils/interface/movie';
import NoImg from '../../assets/no-img.png'
import { Link } from 'react-router-dom';

export const Wishlist = () => {
  const removeMovieFromWishlist = (id: number) => {
    const updatedWishlist = wishlist.filter((movie) => movie.id !== id);
    setWishlist(updatedWishlist);
    console.log(updatedWishlist)
    
    localStorage.setItem("wishlist", JSON.stringify(updatedWishlist));
  };
  
const [wishlist, setWishlist] = useState<MovieModel[]>([]);

useEffect(() => {
  const storedWishlist = localStorage.getItem("wishlist");
  if (storedWishlist) {
    setWishlist(JSON.parse(storedWishlist));
    console.log(wishlist)

  }
  window.scrollTo({ top: 0, behavior: 'smooth' });
}, []);
  return (
    <section id='wishlist'>
        <div className='wishlist container mx-auto px-5 md:px-3'>
            <h1 className='text-2xl'>My Wishlist Movie  </h1>
            {(wishlist.length > 0  ) ? 
                <>
                    <div className="grid grid-cols-2 md:grid-cols-5 lg:grid-cols-7 gap-4 ">
                        {wishlist?.map((movie) => (                        
                            <div key={movie.id} className="">
                                <div className="card">
                                    <div className='card__header'>
                                        <Link to={movie?.first_air_date ? (`/tv/${movie.id}`) :(`/movie/${movie.id}`) }>    
                                            <div className="thumbnail relative">
                                            {movie?.poster_path !== null ? (
                                                <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} alt={ movie.name} className='h-[250px] w-full object-cover'/>
                                                ) : (
                                                <div className='h-[250px] w-full bg-dark-700 flex items-center'>
                                                    <img className='h-auto my-0 mx-auto w-auto object-cover'
                                                    src={NoImg}
                                                    alt={movie.name}
                                                    />
                                                </div>                               
                                                )}  
                                            </div>
                                        </Link>
                                    </div>
                                    <div className='card__body py-3'>
                                        <button className='btn btn__primary w-full rounded-full' onClick={() => removeMovieFromWishlist(movie.id)}>
                                            Remove
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </>
            : 
                <div className='wishlist_blank'>
                    <List/>
                    <h1 className='text-xl my-5'>Oppss..... No Wishlist Movie</h1>
                </div>
            }
        </div>
       
    </section>
  )
}

export default Wishlist