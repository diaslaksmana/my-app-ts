import { Link } from 'react-router-dom';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../../utils/config/Config';
import NoImg from '../../assets/no-img.png'
import Loading from '../component/loading/Loading';
import Load  from '../../assets/loading.gif';
import useFetchKids from '../../hooks/useFetchKids';

export const Kids = () => {
    const {dataKids,isLoading,error}=useFetchKids();
    if (!isLoading) {
      return <Loading/>
    }  
    if (error) {
      return <div>Error: {error}</div>;
    }
  return (
    <div className='kids container mx-auto px-5 md:px-3'>
      <h1 className='text-3xl'>Movie Kids</h1>
      <div className="grid grid-cols-2 md:grid-cols-5 lg:grid-cols-7 gap-4 ">
          {dataKids?.map((movie,index) => (                        
            <div key={index} className="">
              <div className="card">
                <div className='card__header'>
                    <Link to={movie?.first_air_date ? (`/tv/${movie.id}`) :(`/movie/${movie.id}`) }>    
                        <div className="thumbnail relative">
                            {movie?.poster_path !== null ? (
                              <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} alt={ movie.name} className='h-[250px] w-full object-cover'/>
                                ) : (
                              <div className='h-[250px] w-full bg-dark-700 flex items-center'>
                                  <img className='h-auto my-0 mx-auto w-auto object-cover' src={NoImg} alt={movie.name}/>
                              </div>                               
                              )}  
                          </div>
                      </Link>
                  </div>
                </div>
              </div>
            ))}
       </div>
      {isLoading && <div className='load'> <img src={Load}/></div>}
    </div>
  )
}
