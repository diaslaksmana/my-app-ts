import { BACKDROP_SIZE, IMAGE_BASE_URL, POSTER_SIZE } from '../../../utils/config/Config';
import AddIcon from '../../../assets/svg/AddIcon';
import PlayIcon from '../../../assets/svg/PlayIcon';
import { convertRated } from '../../../utils/helper/Helper';
import NoImg from '../../../assets/no-img.png'
import useFetchDetail from '../../../hooks/useFetchDetail';
import Loading from '../../component/loading/Loading';
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";
// import required modules
import { Link } from 'react-router-dom';
import { Autoplay, Navigation,FreeMode, Thumbs  } from "swiper";
import { useState } from 'react';
import { useFetchTvDetail } from '../../../hooks/useFetchTvDetail';
export const Tv = () => {
  const {movie,genresbyId,trailer,cast,review,num,handleMoreImage,isLoading,error,getProgressColor,getRatingPercentage,handleAddToWishlist,handleRemoveFromWishlist,isInWishlist,similar }=useFetchTvDetail();
    const [thumbsSwiper, setThumbsSwiper] = useState<any>(null);
    if (isLoading) {
      return <Loading/>
    }  
    if (error) {
      return <div>Error: {error}</div>;
    }
  
  return (
    <section id='detailPage' className='detailPage'>

    {/* Hero section */}
    
    <div className='detailPage__hero flex justify-center items-center z-10 relative'>
      <img src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${movie?.backdrop_path}`} className="w-full object-top h-full object-cover"/>
      <div className='detailPage__title absolute text-center z-10'>
        <h1 className='text-white text-4xl text-center'>{movie?.name}</h1>
        <div className='detailPage__titleGenre text-white my-3 text-center'>
            {genresbyId?.map((genre) => (
              <span key={genre.id} className="text-xs sm:text-md"> {genre.name}</span> 
            ))}
        </div>
        <p className='text-white text-md text-center mb-4'><b>Release date :</b> {movie?.first_air_date}</p>
      </div>
    </div>
    
    {/* Overview section */}

    <div className='detailPage__overview bg-dark-800 pb-[10px]'>
      <div className='container mx-auto px-5 md:px-0'>
        <div className="block sm:flex flex-row gap-5 mt-[-100px] z-20 relative">
          {/* Left section */}
          <div className="basis-full sm:basis-1/4 mb-5">
            <div className='detailPage__overviewCover'>
            
              <div className="detailPage__overviewRating absolute left-2 top-2 ">
                <svg width="80" height="80">
                  <circle
                    cx="40"
                    cy="40"
                    r="35"
                    stroke="#f1f1f1"
                    strokeWidth="7"
                    fill="none"
                  />
                  <circle
                    cx="40"
                    cy="40"
                    r="35"
                    stroke={getProgressColor(movie?.vote_average)}
                    strokeWidth="7"
                    strokeDasharray={`${getRatingPercentage(movie?.vote_average)} 100`}
                    strokeLinecap="round"
                    fill="none"
                  />
                  <text x="50%" y="60%" textAnchor="middle" fill="#f1f1f1" className='text-2xl font-bold  font-Pridi'>
                    {convertRated(movie?.vote_average)}
                  </text>
                </svg>
              </div>
            
              <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}${movie?.poster_path}`} className='w-full object-cover rounded-lg mb-5'/>
              <div className="grid grid-cols-2 gap-4">
              <div>  
                {isInWishlist ? (
                  <button  className=' w-full btn btn__secondaryOutline py-1 px-5 rounded-full' onClick={handleRemoveFromWishlist} disabled>
                    Already in Wishlist
                  </button>
                ) : (
                  <button className=' w-full btn btn__secondaryOutline py-1 px-5 rounded-full' onClick={handleAddToWishlist}>   <AddIcon/> Add to Wishlist</button>
                )}
              </div>
              <div>
                <a href={movie?.homepage} className=' w-full btn btn__primary py-1 px-5 rounded-full'><PlayIcon/>  Whatching</a>
              </div>
            </div>
            </div>
          </div>
          {/* Right section */}
          <div className="basis-full sm:basis-3/4">
            <h1 className='detailPage__overviewHeading text-2xl text-white'>Overview</h1>
            <p className=' text-md text-white'> {movie?.overview}</p>
            <h1 className='detailPage__overviewHeading text-2xl text-white mt-5'>Cast</h1>
            <ul className='detailPage__overviewCast flex flex-wrap pl-0 ml-0 gap-4 mb-5'>
              {cast?.slice(0, num)?.map((cast,index) => (
                <li key={index}>                                              
                  {cast?.profile_path !== null ? (
                    <img src={"https://image.tmdb.org/t/p/w185/" + cast?.profile_path} alt={cast.name}/>
                      ) : (
                        <div className=' w-full bg-dark-700 flex items-center'>
                          <img className='h-auto my-0 mx-auto w-auto object-cover'
                          src={NoImg}
                          alt={cast.name}
                        />
                      </div>
                    )}
                  <p className='text-white text-xs'>{cast.name}</p>
                </li>
              ))}
                <li>
                    {num < cast?.length && (
                      <button
                        className="load-more font-bold"
                        onClick={ handleMoreImage}
                      >
                        See More
                      </button>
                    )}
                </li>
              </ul>

               {/* Review */}
        
              <h1 className='detailPage__overviewHeading mt-5 text-2xl'>Review</h1>          
              {/* @ts-ignore*/}
              {(review == 0 ) ? <h6 >No review yet</h6>: 
                <div className='detailPage__review'>
                  {review?.map((review) => (               
                    <div className='review__list' key={review.id}>
                      <div className='review__listImg'>
                        {/* @ts-ignore*/}
                        {(review?.author_details?.avatar_path !== null ) ?  <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}${review?.author_details?.avatar_path}`} alt="" />: 
                          <>                        
                          <div className=' w-full bg-dark-700 flex items-center'>
                                <img className='h-auto my-0 mx-auto w-auto object-cover'
                                src={NoImg}
                              />
                            </div>
                          </> 
                        }      
                      </div>
                      <div className='review__listOverview'>
                        <h1>{review?.author}</h1> 
                          {/* @ts-ignore*/}
                        <p>Rated: {review?.author_details?.rating}</p> 
                        <p>{review?.content}</p> 
                      </div>
                    </div>
                  ))}            
                </div>         
              }     

          </div>
        </div>
        
        {/* Thumb gallery */}
        <div className='thumb__gallery'>
        <h1 className='detailPage__overviewHeading text-2xl text-white'>Trailer</h1>
          <Swiper
            loop={true}
            spaceBetween={10}
            navigation={true}
            thumbs={{ swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null }}
            modules={[FreeMode, Navigation, Thumbs]}
            className="thumb_cover"
          >
            {trailer?.slice(0,6).map((trailer) => (
              <SwiperSlide key={trailer.id}>
                <div >
                      <iframe
                                width="100%"
                                height="480"
                                src={"https://www.youtube.com/embed/" + trailer?.key}
                                frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                title="YouTube video player"
                      />        
                  </div>
              </SwiperSlide>
            ))} 
          </Swiper>
          <Swiper
            onSwiper={setThumbsSwiper}
            loop={true}
            spaceBetween={10}
            slidesPerView={4}
            freeMode={true}
            watchSlidesProgress={true}
            modules={[FreeMode, Navigation, Thumbs]}
            className="mySwiper"
          >
            {trailer?.slice(0,6).map((trailer) => (
              <SwiperSlide key={trailer.id}>
                <div >
                      <iframe
                                width="100%"
                                height="200"
                                src={"https://www.youtube.com/embed/" + trailer?.key}
                                frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                title="YouTube video player"
                      />        
                  </div>
              </SwiperSlide>
            ))} 
          </Swiper>
        </div>

        {/* Similliar Movie*/}

        <div className='similar my-10'>
          <div className="block sm:flex  similarrSection">
            <div className=" similarrSection__title">
                 <h1 className="text-white text-4xl font-semibold">
                 Similar Movies<br/>
                  to Watch Now
                  </h1>
                  <p className="text-white text-md py-2 ">Most watched movies by days</p>
            </div>
            <div className="similarrSection_slider">

            <Swiper
             slidesPerView={1.5}
             spaceBetween={30}
             pagination={{
               clickable: true,
             }}
             autoplay={{
               delay: 2500,
               disableOnInteraction: false,
             }}
             breakpoints={{
               640: {
                 slidesPerView: 1.5,
                 spaceBetween: 20,
               },
               768: {
                 slidesPerView: 4,
                 spaceBetween: 40,
               },
               1024: {
                 slidesPerView: 4.5,
                 spaceBetween: 50,
               },
             }}
              navigation={true}
              modules={[Autoplay, Navigation]}
              className="similar__Slider"
            >
               {similar?.map((movie) => (   
              <SwiperSlide key={movie.id}>
                  <div className="card">
                      <div className='card__header'>
                        <Link to={`/tv/${movie.id}`}>    
                            <div className="thumbnail relative">
                                {movie?.poster_path !== null ? (
                                    <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} alt={ movie.name} className='h-[100px] w-full object-cover'/>
                                      ) : (
                                      <div className='h-[300px] w-full bg-dark-700 flex items-center'>
                                        <img className='h-auto my-0 mx-auto w-auto object-cover'src={NoImg} alt={movie.name}/>
                                      </div>                               
                                      )}          
                                <span className='label absolute top-0 right-0 pl-2 pr-[1px] text-center pt-[2px] pb-2 rounded-bl-full font-bold'>{ convertRated(movie.vote_average)}</span>
                              </div>
                          </Link>
                        </div>
                    </div>
              </SwiperSlide>
               ))}
            </Swiper>

            </div>
          </div>
        </div>


      </div>
    </div>
     
  </section>
  )
}

export default Tv
