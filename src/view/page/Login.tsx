import React from 'react'
import Loginimg from '../../assets/login2.jpeg'

export const Login = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  return (
    <section id='login'>
        <div className=''>
            <div className='panelLogin rounded-3xl grid grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-4'>
                <div className='panelLogin__left'>
                    <img src={Loginimg} className=" max-h-[800px] w-full object-cover object-center"/>
                    <h1 className='text-4xl   '>Discover, Stream, <br/>and Experience Movies like Never Before!</h1>
                </div>
                <div className='panelLogin__right'>
                    <h1 className='text-xl font-bold  '>Sign into your account</h1>
                    <form action="" className='formLogin'>
                        <input type="email" placeholder='email' />
                        <input type="password" placeholder='password' />
                        <button className='btn btn__primary'>Login</button>
                    </form>
                    <a href="" className='forgotPass'>Forgot password?</a>
                    <p className="LinkRegister">Don't have an account? <a href="#!" className="text-reset">Register here</a></p>
                    <p className="TnC">
                    <a href="#!">Terms of use.</a> <a href="#!">Privacy policy</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Login