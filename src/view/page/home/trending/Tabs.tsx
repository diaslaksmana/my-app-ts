'use client'
import  { useState } from 'react'
import MovieDays from './movieDays';
import MovieWeek from './movieWeeks';
const TabsMovie = () => {
  const [openTab, setOpenTab] = useState(1);
  return (
    <section id='trending' className='trending'>
      <div className="tabs container  mx-auto sm:px-0 px-5 py-5">
        <div className="w-full">
          <ul
            className="flex items-baseline sm:items-stretch tab__pills"
            role="tablist"
          >
            <li className=' text-left basis-[95%] sm:basis-[90%]'>
                <h1 className='font-bold text-sm sm:text-3xl'>#Trending Movie</h1>
            </li>
            <li className=" text-right flex-auto">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-0 sm:py-3  " +
                  (openTab === 1
                    ? "text-red "
                    : "text-white" )
                }
                onClick={e => {e.preventDefault();setOpenTab(1);}}
                data-toggle="tab"
                href="#link1"
                role="tablist"
              >
                Days
              </a>
            </li>
            <li className=" text-right flex-auto">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-0 sm:py-3  " +
                  (openTab === 2
                    ? "text-red "
                    : "text-white")
                }
                onClick={e => {e.preventDefault();setOpenTab(2);}}
                data-toggle="tab"
                href="#link2"
                role="tablist"
              >
                 Weeks
              </a>
            </li>
          </ul>

          {/*=======================          
                Tab Content 
          ========================*/}

          
          <div className="relative flex flex-col min-w-0 break-wordsw-full mb-6 ">
            <div className=" flex-auto">
              <div className="tabContent tab-space">
                <div className={openTab === 1 ? "show" : "hide"} id="link1">
                    <MovieDays/>
                </div>
                <div className={openTab === 2 ? "show" : "hide"} id="link2">
                   <MovieWeek/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default TabsMovie