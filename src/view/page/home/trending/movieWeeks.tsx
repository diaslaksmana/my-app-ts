import { IMAGE_BASE_URL, POSTER_SIZE } from '../../../../utils/config/Config';
import { convertRated } from '../../../../utils/helper/Helper';
import { Link } from 'react-router-dom';
import useFetchGeneral from '../../../../hooks/useFetchGeneral';
import LoadingSection from '../../../component/loading/LoadingSection';

export const MovieWeek = () => {
  const {trendingWeeks,isLoading,error} = useFetchGeneral();


 
  if (isLoading) {
    return <LoadingSection/>
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <section className="tabPane">
      <div className="sm:grid sm:grid-rows-2 sm:grid-flow-col block gap-4 mx-auto">
        {trendingWeeks?.slice(0,9).map((movie) => (  
            <div  key={movie.id} className="tabPane__list">                               
              <div className="card">
                  <div className="thumbnail relative">
                    <Link to={`/movie/${movie.id}`}>       
                      <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.backdrop_path} alt={ movie.title}  className='h-[200px] w-full object-cover object-center'/>
                      <span className='absolute top-0 right-0 text-white pl-2 pr-[1px] text-center pt-[2px] pb-2 rounded-bl-full bg-yellow font-bold'>{ convertRated(movie.vote_average)}</span>
                      <div className="overview text-xs pl-2 py-2 bg-overlay w-full absolute bottom-0">
                        <p className="text-white">{movie.release_date}</p>
                        <h5 className="text-white font-bold">{movie.title}</h5>
                      </div>
                      </Link>
                  </div>
              </div>         
            </div>
          ))}
      </div>
    </section>
  )
}

export default MovieWeek