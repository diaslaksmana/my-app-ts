import { Link } from 'react-router-dom';
import useFetchGeneral from '../../../hooks/useFetchGeneral';
import LoadingSection from '../../component/loading/LoadingSection';

export const Highlight = () => {
  const {highlight,genresbyId,trailer,isLoading,error} = useFetchGeneral();

  if (isLoading) {
    return <LoadingSection/>
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <section id='highlight' className='flex justify-center items-center'>
      <span className='highlight__label absolute z-10 text-white left-0 h-[200px] text-center bg-dark-700 p-3'>#Trending Tv Series</span>  
        {trailer?.slice(0,1).map((trailer:any) => (
          <div className='highlight__bgCover overflow-hidden h-[600px] relative w-full pb-[56.25%]' key={trailer.id}>
            <iframe 
              className='absolute top-0 left-0 w-full h-full border-0'             
              src={"https://www.youtube.com/embed/" + trailer?.key + "?controls=0&autoplay=1&mute=1&loop=1&playlist="+ trailer?.key }
              title="YouTube video player" frameBorder="0" 
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
              allowFullScreen>
            </iframe>
          </div>
        ))} 
      <div className=' highlight__overview  absolute text-center'>
        {/* @ts-ignore*/}
        <h1 className='text-white text-3xl'>{highlight.name}</h1>
        {/* @ts-ignore*/}
        <p className='text-white text-sm'>{highlight?.overview}</p>
        {/* @ts-ignore*/}
        <p className='text-white text-sm'> Season {highlight?.number_of_seasons} || <b>Last updated</b> {highlight?.last_episode_to_air?.air_date}</p>
        <div className='highlight__overviewGenre text-white my-3 mb-10'>
          {genresbyId?.map((genre:any) => (
            <span key={genre.id} className="text-xs sm:text-md"> {genre.name}</span> 
          ))}
        </div>
         {/* @ts-ignore*/}
        <Link to={`/tv/${highlight?.id}`} className=' py-3 px-5 mt-5 btn btn__alt rounded'>Watching Now</Link>
      </div>      
    </section>
  )
}

export default Highlight