import React from 'react'
import { Link } from 'react-router-dom';
import useFetchGeneral from '../../../../hooks/useFetchGeneral';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../../../../utils/config/Config';
import LoadingSection from '../../../component/loading/LoadingSection';

export const UpcomingTv = () => {
    const {upcomingTv,isLoading,error} = useFetchGeneral();

    if (isLoading) {
      return <LoadingSection/>
    }
  
    if (error) {
      return <div>Error: {error}</div>;
    }
  return (
    <section id='upcoming' className='bg-[#131722]'>
        <div className='container mx-auto px-5 md:px-0'>
            <h1 className="text-white text-4xl font-semibold">On Trending Series</h1>
            <div className='block sm:flex gap-5'>
                {upcomingTv?.slice(0,10).map((movie) => (   
                    <div className="box">
                        <Link to={`/tv/${movie.id}`}>    
                        </Link>
                            <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} alt={ movie.name} className='object-cover'/>
                    </div>
                ))}
            </div>
        </div>
    </section>
  )
}

export default UpcomingTv