import { useEffect } from 'react'
import Banner, { Hero } from '../../layout/Hero'
import Highlight from './Highlight'
import LoginSection from './LoginSection'
import Popular from './popular/Popular'
import PopularTv from './popular/PopularTv'
import TabsMovie from './trending/Tabs'
import UpcomingMovie from './upcoming/UpcomingMovie'
import UpcomingTv from './upcoming/UpcomingTv'

export const Home = () => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
    }, []);
  return (
    <div>
      <Hero/>
      <Popular/>
      <TabsMovie/>
      <UpcomingMovie/>
      <PopularTv/>
      <UpcomingTv/>
      <Highlight/>
      <LoginSection/>
    </div>
  )
}

export default Home