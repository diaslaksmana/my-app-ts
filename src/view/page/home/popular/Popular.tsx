import { IMAGE_BASE_URL, POSTER_SIZE } from '../../../../utils/config/Config';
import { convertRated } from '../../../../utils/helper/Helper';
import NoImg from '../../../../assets/no-img.png'
import { Link } from 'react-router-dom';
import useFetchGeneral from '../../../../hooks/useFetchGeneral';
import LoadingSection from '../../../component/loading/LoadingSection';
export const Popular = () => {
 
  const {popular,isLoading,error} = useFetchGeneral();

  if (isLoading) {
    return <LoadingSection/>
  }

  if (error) {
    return <div>Error: {error}</div>;
  }


  return (
   
    <section className="popularSection bg-dark-900 py-10">
        <div className="container mx-auto px-5 md:px-0">
            <div className="grid grid-cols-2 md:grid-cols-7 gap-4 items-center">
                <div className="col-span-2  popularSection__title">
                    <h1 className="text-white text-4xl font-semibold">
                    Popular Movies<br/>
                    to Watch Now
                    </h1>
                    <p className="text-white text-md py-2 ">Most watched movies by days</p>
                </div>
                {/* @ts-ignore*/}
                {popular?.slice(0,12).map((movie) => (                        
                <div key={movie.id} className="">
                    <div className="card">
                      <div className='card__header'>
                        <Link to={`/movie/${movie.id}`}>    
                            <div className="thumbnail relative">
                            {movie?.poster_path !== null ? (
                                  <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} alt={ movie.name} className='h-[200px] w-full object-cover'/>
                                ) : (
                                  <div className='h-[200px] w-full bg-dark-700 flex items-center'>
                                      <img className='h-auto my-0 mx-auto w-auto object-cover'
                                      src={NoImg}
                                      alt={movie.name}
                                    />
                                  </div>                               
                                )}          
                                <span className='label absolute top-0 right-0 pl-2 pr-[1px] text-center pt-[2px] pb-2 rounded-bl-full font-bold'>{ convertRated(movie.vote_average)}</span>
                            </div>
                          </Link>
                      </div>
                        <div className='card__body'>
                            <p className='font-bold '>{movie.release_date}</p>
                            <h1 className='text-xs '>{movie.title}</h1>
                        </div>
                    </div>
                </div>
                ))}


            </div>
        </div>
    </section>
   
  )
}

export default Popular