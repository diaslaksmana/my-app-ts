import React from 'react'
import { Link } from 'react-router-dom'

export const LoginSection = () => {
  return (
    <section className='bg-dark-800 py-8'>
        <div className='container mx-auto text-center'>
            <h1 className='text-white text-3xl mb-5'>Ready to watch? Login to your account now.</h1>
            <Link to='' className='btn btn__primary py-2 px-8 rounded'>Login now</Link>
        </div>
    </section>
  )
}

export default LoginSection