import React from 'react'
import { Link } from 'react-router-dom'
import NotFoundIcon from '../../assets/svg/NotFoundIcon'
export const NotFound = () => {
  return (
    <div className='notfound bg-dark-800'>
        <NotFoundIcon/>
        <h1 className='text-4xl mb-5 text-white'>Oppsss....<b>404</b> Page Not Found</h1>
        <Link to='/' className='text-white bg-red py-2 px-5 rounded-full'>Back to Home </Link>
    </div>
  )
}
export default NotFound
