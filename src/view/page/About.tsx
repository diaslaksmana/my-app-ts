import React from 'react'
function Hero() {
  const parallax = React.useRef<any>(null);

  React.useEffect(() => {
    function scrollHandler() {
      const element = parallax.current;
      if (element) {
        let offset = window.pageYOffset;
        element.style.backgroundPositionY = offset * 0.12 + "px";
      }
    }
    window.addEventListener("scroll", scrollHandler);
    // return the function to remove the handler
    // which will be run whn Hero is unmounted
    return () => window.removeEventListener("scroll", scrollHandler);
  }, []);

  return (
    <div className="hero-container" id="home" ref={parallax}>
      <h1 className="text-white text-4xl z-10 fixed">About <span>movie</span></h1>
    </div>
  );
}

export const About = () => {
  return (
   <section id='about'>
      <Hero />
      <div className='about z-20 relative '>
        <div className='container mx-auto px-5 py-5 sm:px-0 '>
          <p className='text-center'>
            Welcome to Movie, your ultimate destination for all things movies! Our website is designed to provide you with an exceptional movie-watching experience, where you can explore, discover, and enjoy the latest and greatest films from around the world.
            With an intuitive and user-friendly interface, Movie allows you to easily navigate through our vast collection of movies, ensuring you find the perfect film for any mood or occasion. Whether you're in the mood for an action-packed blockbuster, a heartwarming romance, a thrilling suspense, or a thought-provoking documentary, we have something for everyone.
            <br/><br/>
            Discover new releases and stay up-to-date with the hottest trends in the movie industry. Our comprehensive movie database is constantly updated with the latest titles, ensuring you never miss out on the most anticipated films of the year. From Hollywood blockbusters to independent gems, we curate a diverse selection of movies to cater to every taste and preference.
            <br/><br/>
            Movie goes beyond simply providing a list of movies. We understand that each viewer has unique preferences, which is why our personalized recommendation engine suggests movies tailored specifically to your interests. Based on your viewing history and ratings, we offer personalized movie suggestions that are sure to keep you engaged and entertained.
            Explore in-depth movie details, including trailers, synopses, cast and crew information, and user reviews. Our website is designed to provide you with all the information you need to make an informed decision about what to watch. Additionally, you can create and share your own movie lists, allowing you to recommend your favorite films to friends and family.
            <br/><br/>
            Join our vibrant movie-loving community, where you can discuss your favorite films, engage in lively debates, and connect with fellow movie enthusiasts. Movie fosters a sense of community, providing a platform for movie lovers to interact, share recommendations, and engage in conversations about all things cinema.
            Experience the magic of movies like never before with Movie. Whether you're looking for a thrilling escape, a heartwarming story, or an eye-opening documentary, our website is your one-stop destination for all your movie-watching needs. Sit back, relax, and let Movie be your guide to the wonderful world of cinema. Start your movie adventure today!
          </p>
        </div>  
      </div>      
   </section>
  )
}

export default About






