
import { Route, Routes } from "react-router";
import About from "../view/page/About";
import { MovieCategory } from "../view/page/category/MovieCategory";
import { TvCategory } from "../view/page/category/TvCategory";
import Movie from "../view/page/detail/MoviePage";
import Tv from "../view/page/detail/TvPage";
import Home from "../view/page/home/Home";
import { Kids } from "../view/page/Kids";
import Login from "../view/page/Login";
import NotFound from "../view/page/NotFound";
import Wishlist from "../view/page/Wishlist";

const Router = () => {
    return (
      <Routes >
              <Route  path="" element={<Home />}/>
              <Route  path="home" element={<Home />}/>
              <Route  path="about" element={<About />}/>
              
              <Route  path="*" element={<NotFound />}/>
                {/* @ts-ignore*/}
              <Route path="/movie" element={<Movie />}>
                <Route  path=":id"/>
                <Route  path=":id/page/:num"/>
              </Route>
              <Route path="/tv" element={<Tv />}>
                <Route  path=":id"/>
                <Route  path=":id/page/:num"/>
              </Route>

            <Route  path="wishlist" element={<Wishlist />}/>
            <Route  path="kids" element={<Kids />}/>
            
            <Route  path="moviecategory" element={<MovieCategory />}>
              <Route  path=":id"/>
              <Route  path=":id/page/:num"/>
            </Route>

            <Route  path="tvcategory" element={<TvCategory />}>
              <Route  path=":id"/>
              <Route  path=":id/page/:num"/>
            </Route>

            <Route  path="login" element={<Login />}/>
      </Routes>
    )
  }
  export default Router;