import { API_KEY, BASE_URL, options } from "../config/Config";

export async function fetchData(endpoint: string) {
  //const apikey= process.env.REACT_APP_API_KEY;
    const response = await fetch(`${BASE_URL}/${endpoint}?api_key=${API_KEY}&language=en-US,${options}`);
    const data = await response.json();
    if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
    return data;
  }

  export async function fetchDataValue(endpoint: string, value:string, num:number) {
    const apikey= process.env.REACT_APP_API_KEY;
      const response = await fetch(`${BASE_URL}/${endpoint}?api_key=${apikey}&language=en-US&query=${value}&page=${num},${options}`);
      const data = await response.json();
      if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
      return data;
    }
