export interface ReviewModel{
    id:number;
    author:string;
    name:string;
    avatar_path:string;
    content:string;
    rating:number;
    author_details:string;
}
