// Configuration for TMDB API
// Read more about the API here: https://developers.themoviedb.org/

const BASE_URL: string  = "https://api.themoviedb.org/3/"
const  API_KEY: string  = "45bf6592c14a965b33549f4cc7e6c664"

const IMAGE_BASE_URL: string = 'http://image.tmdb.org/t/p/'
// Sizes: w300, w780, w1280, original
const BACKDROP_SIZE: string = 'original/'
// w92, w154, w185, w342, w500, w780, original
const POSTER_SIZE: string = "w780/"
// w92, w154, w185, w342, w500, w780, original
const PROFILE_SIZE: string = "w185/"
const options = {
    method: 'GET',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    }      
  };
export {
  API_KEY,
  BASE_URL,
  IMAGE_BASE_URL,
  BACKDROP_SIZE,
  POSTER_SIZE,
  PROFILE_SIZE,
  options
}
