import { useEffect, useState } from 'react'
import { MovieModel } from '../utils/interface/movie';
import { fetchDataValue } from '../utils/service/movie.service';

export const useFetchSearch = () => {
    const [data, setData] = useState<MovieModel[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<any>();
    const [value, setValue] = useState("");
    
    const fetchSearch = ()=>{
        async function getData() {
            try{
              const result = await fetchDataValue('search/multi','value',1);
              setData(result.results);
            }catch (error) {
                setError(error);
            } finally {
                setIsLoading((false));
            }
          }
        getData();
    }
    useEffect(() => {
        fetchSearch()
    }, []); 
    return { data,isLoading,error,value,setValue};
}

export default useFetchSearch