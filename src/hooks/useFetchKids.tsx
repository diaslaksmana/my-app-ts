import React, { useEffect, useState } from 'react'
import { BASE_URL, options } from '../utils/config/Config';
import { MovieModel } from '../utils/interface/movie';

export const useFetchKids = () => {
   //Kids section
   const [page, setPage] = useState<number>(1);
   const [dataKids, setDataKids] = useState<MovieModel[]>([]);
   const [isLoading, setIsLoading] = useState(false);
   const [error, setError] = useState<any>();
   const apikey= process.env.REACT_APP_API_KEY;
   
   useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
      loadMovies();
    }, []);
    
    const loadMovies = async () => {
      try {
          const response = await fetch(`${BASE_URL}/discover/tv?api_key=${apikey}&language=en-US&page=${page}&with_genres=10762,${options}`);            
          const data = await response.json();
          setDataKids((prevMovies) => [...prevMovies, ...data.results]);
          console.log(data)
          setPage((prevPage) => prevPage + 1);
      }catch (error) {
          setError(error);
        } finally {
          setIsLoading(true);
        }
  }
 

    // Add an event listener to track the scroll position
    useEffect(() => {
    const handleScrollPage = () => {
        if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
        loadMovies();
        }
    };

    window.addEventListener('scroll', handleScrollPage);
    return () => {
        window.removeEventListener('scroll', handleScrollPage);
    };
    }, [page]);
    
    return{page,dataKids,isLoading,error}
}

export default useFetchKids
