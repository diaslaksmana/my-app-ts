import React, { useEffect, useState } from 'react'
import { BASE_URL, options } from '../utils/config/Config';
import { MovieModel } from '../utils/interface/movie';

export const useFetchHeader = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const [data, setData] = useState<MovieModel>();
    const [value, setValue] = useState<string>("");
    const[error, setError]= useState<any>();
    const[isLoading, setIsLoaded]=useState(true);
    const apikey= process.env.REACT_APP_API_KEY;
    let num =1;
    const fetchTv =()=>{
      async function getData() {
          try{
            const response = await fetch(`${BASE_URL}search/multi?api_key=${apikey}&language=en-US&query=${value}&page=${num}&include_adult=true`,options);
            const data = await response.json();            
            setData(data.results);
            console.log(data.results)
          }catch (error) {
              setError(error);
          } finally {
            setIsLoaded((false));
          }
        }
        getData();
    }

  useEffect(() => {
    fetchTv()
    
    const handleScroll = () => {
      const scrollTop = window.pageYOffset;
      if (scrollTop > 0) {
        setIsScrolled(true);
      } else {
        setIsScrolled(false);
      }
    };

    window.addEventListener('scroll', handleScroll);

    // Clean up function to remove event listener
    return () => window.removeEventListener('scroll', handleScroll);
  }, [value]);

  useEffect(() => {
    document.body.classList.toggle('scrolled', isScrolled);
  }, [isScrolled]);

  const onChange = (event:any) => {
    setValue(event.target.value);
  };
  return{isScrolled,data,value,onChange}

}

export default useFetchHeader
