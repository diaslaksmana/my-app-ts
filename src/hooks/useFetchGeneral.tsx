import { useEffect, useState } from 'react'
import { GetMoviesResponse, MovieModel } from '../utils/interface/movie';
import { TrailerModel } from '../utils/interface/trailer';
import { fetchData } from '../utils/service/movie.service';

export const useFetchGeneral = () => {

    const [upcoming, setUpcoming] = useState<MovieModel[]>([]);
    const [popular, setPopular] = useState<MovieModel[]>([]);
    const [popularTv, setPopularTv] = useState<MovieModel[]>([]);
    const [trendingDay, setTrendingDay] = useState<MovieModel[]>([]);
    const [trendingWeeks, setTrendingWeeks] = useState<MovieModel[]>([]);
    const [upcomingMovie, setUpcomingMovie] = useState<MovieModel[]>([]);
    const [upcomingTv, setUpcomingTv] = useState<MovieModel[]>([]);
    const [highlight, setHighlight] = useState<MovieModel[]>([]);
    const[genresbyId, setGenresbyId]=useState<GetMoviesResponse[]>([]);
    const[trailer, setTrailer]=useState<TrailerModel[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<any>();

   

    // Hero section
    const fetchHero =()=>{
        async function getData() {
            try{
              const result = await fetchData('trending/all/week');
              setUpcoming(result.results);
            }catch (error) {
                setError(error);
            } finally {
                setIsLoading((false));
            }
          }
        getData();
    }

    // Popular section
    const fetchPopularMovie =()=>{
      async function getData() {
          try{
            const result = await fetchData('movie/popular');
            setPopular(result.results);
          }catch (error) {
              setError(error);
          } finally {
              setIsLoading((false));
          }
        }
      getData();
    }
    const fetchPopularTv =()=>{
      async function getData() {
        try{
          const result = await fetchData('tv/top_rated');
          setPopularTv(result.results);
        }catch (error) {
            setError(error);
          } finally {
            setIsLoading((false));
          }        
        }
      getData();
    }

     // Popular Upcoming Movie
     const fetchUpcomingMovie =()=>{
      async function getData() {
          try{
            const result = await fetchData('movie/upcoming');
            setUpcomingMovie(result.results);
          }catch (error) {
              setError(error);
          } finally {
              setIsLoading((false));
          }
        }
      getData();
    }
    

      // Popular Upcoming Tv
      const fetchUpcomingTv =()=>{
        async function getData() {
            try{
              const result = await fetchData('trending/tv/day');
              setUpcomingTv(result.results);
            }catch (error) {
                setError(error);
            } finally {
                setIsLoading((false));
            }
          }
        getData();
      }
        
    // Trending section
    const fetchTrendingDay =()=>{
      async function getData() {
          try{
            const result = await fetchData('trending/movie/day');
            setTrendingDay(result.results);
          }catch (error) {
              setError(error);
            } finally {
              setIsLoading((false));
            }          
        }
      getData();
    }
    const fetchTrendingWeeks =()=>{
      async function getData() {
          try{
            const result = await fetchData('trending/movie/week');
            setTrendingWeeks(result.results);
          }catch (error) {
            setError(error);
          } finally {
            setIsLoading((false));
          }          
        }
      getData();
    }

    // Highlight
    const fetchHighlight =()=>{
      async function getData() {
        try{
          const result = await fetchData('tv/100088');
          setHighlight(result);
          setGenresbyId(result.genres)      
          console.log(result)      
          const trailer = await fetchData('tv/100088/videos');
          setTrailer(trailer.results);
        }catch (error) {
          setError(error);
        } finally {
          setIsLoading((false));
        }           
      }    
      getData();
    }

    useEffect(() => {
      fetchHero()
      fetchPopularMovie()
      fetchPopularTv()
      fetchTrendingDay()
      fetchTrendingWeeks()
      fetchHighlight()
      fetchUpcomingMovie()
      fetchUpcomingTv()
      }, []); 
      return { upcoming,popular,popularTv,trendingDay,trendingWeeks,highlight,genresbyId,trailer,isLoading,error,upcomingTv,upcomingMovie };
}

export default useFetchGeneral