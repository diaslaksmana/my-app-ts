import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import { BASE_URL, options } from '../utils/config/Config';
import { MovieModel } from '../utils/interface/movie';
import { fetchData } from '../utils/service/movie.service';

export const useFetchCategory = () => {
  const params = useParams();
  const[error, setError]= useState<any>();
  const[isLoading, setIsLoaded]=useState(true);
  const[movies, setMovies]=useState<MovieModel>();
  const[genres, setGenres]=useState<any>();
  const[tv, setTv]=useState<MovieModel>();
  const[genresTv, setGenresTv]=useState<any>();
  const [pages, setPages] = useState<any>();
  const [num, setNum] = useState(1);
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: "smooth"
  });
  const handleGenre = () => {
    setNum(1)
  };
  const apikey= process.env.REACT_APP_API_KEY;

  const fetchMovie =()=>{
    async function getData() {
        try{
          const response = await fetch(`${BASE_URL}/discover/movie?api_key=${apikey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${num}&with_genres=${params.id}`,options);
          const data = await response.json();            
          setMovies(data.results);
          console.log(data.results)
          setPages(data);  
          const response2 = await fetch(`${BASE_URL}/genre/movie/list?api_key=${apikey}&language=en-US`,options);
          const data2 = await response2.json();            
          setGenres(data2.genres);
        }catch (error) {
            setError(error);
        } finally {
          setIsLoaded((false));
        }
      }
    getData();
}

const fetchTv =()=>{
  async function getData() {
      try{
        const response = await fetch(`${BASE_URL}/discover/tv?api_key=${apikey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${num}&with_genres=${params.id}`,options);
        const data = await response.json();            
        setTv(data.results);
        console.log(data.results)
        setPages(data);  
        const response2 = await fetch(`${BASE_URL}/genre/tv/list?api_key=${apikey}&language=en-US`,options);
        const data2 = await response2.json();            
        setGenresTv(data2.genres);
      }catch (error) {
          setError(error);
      } finally {
        setIsLoaded((false));
      }
    }
    getData();
}

useEffect(() => {
  fetchMovie()
  fetchTv()
  }, [num,params.id]); 

  return {error,isLoading,movies,genres,handleGenre,pages,num, setNum,tv,genresTv}
}
