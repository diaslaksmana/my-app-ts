import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import { CastModel } from '../utils/interface/cast';
import { GetMoviesResponse, MovieModel } from '../utils/interface/movie';
import { ReviewModel } from '../utils/interface/review';
import { TrailerModel } from '../utils/interface/trailer';
import { fetchData } from '../utils/service/movie.service';

const imagePerRow = 4;
export const useFetchTvDetail = () => {
    const params = useParams();
    // Movie
    const [movie, setMovie] = useState<MovieModel>();
    const[genresbyId, setGenresbyId]=useState<GetMoviesResponse[]>([]);
    const[trailer, setTrailer]=useState<TrailerModel[]>([]);
    const [cast, setCast] = useState<CastModel[]>([]);
    const [review, setReview] = useState<ReviewModel[]>([]);
    const [similar, setSimilar] = useState<MovieModel[]>([]);
   

    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<any>();
    const [num, setNum] = useState(8);
    const [isInWishlist, setIsInWishlist] = useState(false);

    useEffect(() => {
      const storedWishlist = localStorage.getItem("wishlist");
      if (storedWishlist) {
        const parsedWishlist = JSON.parse(storedWishlist);
        setIsInWishlist(parsedWishlist.some((m: MovieModel) => m.id === movie?.id));
      }
    }, [movie]);

    const handleAddToWishlist = () => {
      const storedWishlist = localStorage.getItem("wishlist");
      if (storedWishlist) {
        const parsedWishlist = JSON.parse(storedWishlist);
        parsedWishlist.push(movie);
        localStorage.setItem("wishlist", JSON.stringify(parsedWishlist));
      } else {
        localStorage.setItem("wishlist", JSON.stringify([movie]));
      }
      setIsInWishlist(true);
    };

    const handleRemoveFromWishlist = () => {
      const storedWishlist = localStorage.getItem("wishlist");
      if (storedWishlist) {
        const parsedWishlist = JSON.parse(storedWishlist);
        const newWishlist = parsedWishlist.filter((m: MovieModel) => m.id !== movie?.id);
        localStorage.setItem("wishlist", JSON.stringify(newWishlist));
        setIsInWishlist(false);
      }
    };

    // add more image cast
    const handleMoreImage = () => {
      setNum(num + imagePerRow);
    };

    // ratting
    const getProgressColor = (voteAverage: number=0) => {
        if (voteAverage >= 7.5) {
          return '#21d07a';
        } else if (voteAverage >= 5.0) {
          return '#f2c94c';
        } else {
          return '#eb5757';
        }
      };
  
      const getRatingPercentage = (voteAverage: number=0) => {
        return (voteAverage / 10) * 100;
      };

    // Movie Detail
    const fetcMovie = () =>{
        async function getData() {
           try{
            const result = await fetchData(`tv/${params.id}`);
            setMovie(result);
            setGenresbyId(result.genres)      
            console.log(result)      
            const trailer = await fetchData(`tv/${params.id}/videos`);
            setTrailer(trailer.results);
            const cast = await fetchData(`tv/${params.id}/credits`);
            setCast(cast.cast);
            const review = await fetchData(`tv/${params.id}/reviews`);
            setReview(review.results);
            console.log(review)   
            const similar = await fetchData(`tv/${params.id}/similar`);
            setSimilar(similar.results);

           }  catch (error) {
                setError(error);
            } finally {
                setIsLoading((false));
            }
            
          }
                
          getData();
    }


    useEffect(() => {
      fetcMovie()    
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }, [params.id]); 

    return {movie,genresbyId,trailer,cast,review,num,handleMoreImage,isLoading,error,getProgressColor,getRatingPercentage,handleAddToWishlist,handleRemoveFromWishlist,isInWishlist,similar  };

}
