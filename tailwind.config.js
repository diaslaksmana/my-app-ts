/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        Pridi: ["Pridi", "serif"],
        Poppins: ["Poppins", "sans-serif"],
       },
    },
    colors:{
      'overlay':'rgb(0,0,0,0.5)',
      'black':'#000',
      'red':'#E50914',
      'yellow':'#ffac42',
      'white':'#fff',
      'dark':{
        900:'#0e0d12',
        800:'#131722',
        700:'#0f172a'
      }
    }
  },
  plugins: [],
}